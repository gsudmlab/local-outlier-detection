# Detecting Local Outliers from Spatio-temporal Trajectories in SolarEvent Datasets

This project aims to detect local outliers (outlying trajectory segments) from Spatio-temporal Trajectories in SolarEvent Datasets (i.e., solar active region, CME, PIL evolution)

### Requirements:
* Python >= 3.6
* Environment setting: [environment_py.yml](environment_py.yml )
----
## Data Source:

NOAA datasets [noaa data](/Data/NOAA data/)

Coronal Mass Ejection (CME) datasets [cme data](/Data/CME data/)

Plarity Inversion Lines (PIL) evolution datasets [pil data](/Data/PIL evolution data/)

----
## Mehodology:

I. Temporal Partition and Feature Extraction
Devide TR into a list of {ts_0, ts_1, ..., ts_n}

A. Temporal Partion

a. NOAA and PIL evolution use periodic sampling interval partition strategty.

b. CME dataset use non-periodic sampling interval partition strategy.[CME Notebook](CME_trajectory.ipynb)
```python
def optimal_time_interval(trj,u_1,tt,u_2,k)
```
```python
def idx_in_bin(trj,t_delta,unit,attr,m_batch)
```

B. Feature extracted for each dataset:

a. NOAA [NOAA Notebook](NOAA_trajectory_outlier_detection.ipynb)

Longitude displacement, Latitude displacement, Displacement Vector Magnitude, Displacement Vector Direction
```python
def trj_seg_spatial(trj,trj_id)
```

b. CME[CME Notebook](CME_trajectory.ipynb)

Average Velocity (Height), Average Acceleration (Height), Time-normalized Cumulative Angle Displacement

Average Velocity (Height)
```python
def feature_generation_avg_velocity(trj, idx_list, attr,unit='h')
```
Average Acceleration (Height)
```python
def feature_generation_acce_inst_v_2(trj, idx_list, attr,unit='h')
```
Time-normalized Cumulative Angle Displacement
```python
def feature_generation_Angle(trj, idx_list, attr,unit)
```
c. PIL evolution [PIL Notebook](PIL_evolution_outlier_detection.ipynb)

PIL  size change, Change in unsigned flux around the PIL, Change in fractal dimension of PIL
```python
def trj_seg_spatial(trj,trj_id)
```

II. Clustering and Template Generation

Apply MinMax normalization to feature of trajectory segments.
```python
MinMaxScaler()
```

Select number of K clusters to generate template ts.
```python
KMeans(n_clusters=3).fit(data_transformed)
```

III. Dissmilarity Comparison

AB score implementation
```python
def abScore(data,label,c_center):
```

Determin AB threshold by distribution of AB score of ts depend on datasets.


----
## Detecting Results for Three Datasets:

1. NOAA
[NOAA Notebook](NOAA_trajectory_outlier_detection.ipynb)
![NOAA_detection result](https://bitbucket.org/gsudmlab/local-outlier-detection/raw/2d0c1b9584d6fc58d0b3ec567aa629f57501df93/Plots/NOAA%20active%20region%20figure/NOAA_result.png){:height="36px" width="36px"}



2. CME
[CME Notebook](CME_trajectory.ipynb)

Normal trajectory segments
![CME normal ts](https://bitbucket.org/gsudmlab/local-outlier-detection/raw/2d0c1b9584d6fc58d0b3ec567aa629f57501df93/Plots/CME%20figure/CME_normal_single.png){:height="50%" width="50%"}

Abnormal trajectory segments with decelaration
![CME abnormal ts_cluster0](https://bitbucket.org/gsudmlab/local-outlier-detection/raw/2d0c1b9584d6fc58d0b3ec567aa629f57501df93/Plots/CME%20figure/CME_ab_0_single.png){:height="50%" width="50%"}

Abnormal trajectory segments with zigzag moving pattern
![CME abnormal ts_cluster2](https://bitbucket.org/gsudmlab/local-outlier-detection/raw/2d0c1b9584d6fc58d0b3ec567aa629f57501df93/Plots/CME%20figure/CME_ab_2_single.png){:height="50%" width="50%"}

Abnormal trajectory segments with high velocity and high acceleration
![CME abnormal ts_cluster1](https://bitbucket.org/gsudmlab/local-outlier-detection/raw/2d0c1b9584d6fc58d0b3ec567aa629f57501df93/Plots/CME%20figure/CME_ab_1_single.png){:height="50%" width="50%"}

3. PIL
[PIL Notebook](PIL_evolution_outlier_detection.ipynb)
![PIL detection result](https://bitbucket.org/gsudmlab/local-outlier-detection/raw/2d0c1b9584d6fc58d0b3ec567aa629f57501df93/Plots/PIL%20figure/PIL_01_2000_AB_score.png)


